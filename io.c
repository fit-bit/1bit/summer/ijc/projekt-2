// io.c
// Řešení IJC-DU2, příklad 2), 25.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// funkce get_word která čte jedno slovo ze souboru f do zadaného pole znaků a vrátí délku slova 
// (z delších slov načte prvních max-1 znaků, a zbytek přeskočí)

#include <stdbool.h>
#include <ctype.h>
#include <stdio.h>

int get_word(char *s, int max, FILE *f)
{
    // funkce vrátí -2 když došlo k přetečení
    bool print_err = false;

    int word_length = 0;

    // Čtení souboru po znacích
    int ch;
    while ((ch = fgetc(f)) != EOF && isspace(ch))
    {
        // zahodí se znaky špatné znaky
    }

    if (ch == EOF)
    {
        return EOF;
    }

    // Uložení prvního znaku který už se načetl
    s[word_length] = ch;
    word_length++;

    // Ukládání znaků
    while ((ch = fgetc(f)) != EOF && !isspace(ch))
    {
        if (word_length == max - 1)
        {
            print_err = true;
            break;
        }
    
        s[word_length] = ch;
        word_length++;
    }

    // Pokud se slovo useklo musí se zahodit zbylé znaky
    while (print_err == true && (ch = fgetc(f)) != EOF && !isspace(ch))
    {
        // zahození znaků
    }

    if (print_err == true)
    {
        return -2;
    }

    if (ch == EOF)
    {
        return EOF;
    }

    return 0;
}