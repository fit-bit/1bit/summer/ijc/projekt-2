# Makefile
# Řešení IJC-DU2, příklad 1),2), 25.3.2019
# Autor: Matěj Kudera, FIT
# Přeloženo: gcc 7.4
# 	     g++ 7.4
# Makefile pro složení programu

# Potřebné parametry
C = gcc
FLAGS_C = -std=c99 -pedantic -Wall -Wextra

C++ = g++
FLAGS_C++ = -std=c++11 -pedantic -Wall

all: tail tail2 wordcount wordcount-dynamic libhtab.a libhtab.so

############## Výsledné programy ##############

tail: tail.o
	$(C) $(FLAGS_C) tail.o -o tail

tail2: tail2.o
	$(C++) $(FLAGS_C++) tail2.o -o tail2

wordcount: wordcount.o io.o libhtab.a
	$(C) $(FLAGS_C) wordcount.o io.o libhtab.a -o wordcount

# export nefunguje, potřeba pad dodat
wordcount-dynamic: wordcount.o io.o libhtab.so
	export LD_LIBRARY_PATH="."
	$(C) $(FLAGS_C) wordcount.o io.o libhtab.so -o wordcount-dynamic

############# Knihovny sestavení dohromady ##############

libhtab.a: htab_hash_function.o htab_init.o htab_move.o htab_size.o htab_bucket_count.o htab_lookup_add.o htab_begin.o htab_end.o htab_iterator_next.o htab_iterator_get_key.o htab_iterator_get_value.o htab_iterator_set_value.o htab_clear.o htab_free.o
	ar rcs libhtab.a htab_hash_function.o htab_init.o htab_move.o htab_size.o htab_bucket_count.o htab_lookup_add.o htab_begin.o htab_end.o htab_iterator_next.o htab_iterator_get_key.o htab_iterator_get_value.o htab_iterator_set_value.o htab_clear.o htab_free.o

libhtab.so: htab_hash_function_shared.o htab_init_shared.o htab_move_shared.o htab_size_shared.o htab_bucket_count_shared.o htab_lookup_add_shared.o htab_begin_shared.o htab_end_shared.o htab_iterator_next_shared.o htab_iterator_get_key_shared.o htab_iterator_get_value_shared.o htab_iterator_set_value_shared.o htab_clear_shared.o htab_free_shared.o
	$(C) $(FLAGS_C) -shared htab_hash_function_shared.o htab_init_shared.o htab_move_shared.o htab_size_shared.o htab_bucket_count_shared.o htab_lookup_add_shared.o htab_begin_shared.o htab_end_shared.o htab_iterator_next_shared.o htab_iterator_get_key_shared.o htab_iterator_get_value_shared.o htab_iterator_set_value_shared.o htab_clear_shared.o htab_free_shared.o -o libhtab.so

############ Dependence tail a tail2 ############

tail.o: tail.c
	$(C) $(FLAGS_C) -c tail.c -o tail.o

tail2.o: tail2.cc
	$(C++) $(FLAGS_C++) -c tail2.cc -o tail2.o

############ Dependence wordcount a wordcount-dynamic ############

wordcount.o: wordcount.c htab.h
	$(C) $(FLAGS_C) -c -fPIC wordcount.c -o wordcount.o

io.o: io.c
	$(C) $(FLAGS_C) -c -fPIC io.c -o io.o

############ Statická knihovna části ##########

htab_hash_function.o: htab_hash_function.c
	$(C) $(FLAGS_C) -c htab_hash_function.c -o htab_hash_function.o

htab_init.o: htab_init.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_init.c -o htab_init.o

htab_move.o: htab_move.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_move.c -o htab_move.o

htab_size.o: htab_size.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_size.c -o htab_size.o

htab_bucket_count.o: htab_bucket_count.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_bucket_count.c -o htab_bucket_count.o

htab_lookup_add.o: htab_lookup_add.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_lookup_add.c -o htab_lookup_add.o

htab_begin.o: htab_begin.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_begin.c -o htab_begin.o

htab_end.o: htab_end.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_end.c -o htab_end.o

htab_iterator_next.o: htab_iterator_next.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_iterator_next.c -o htab_iterator_next.o

htab_iterator_get_key.o: htab_iterator_get_key.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_iterator_get_key.c -o htab_iterator_get_key.o

htab_iterator_get_value.o: htab_iterator_get_value.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_iterator_get_value.c -o htab_iterator_get_value.o

htab_iterator_set_value.o: htab_iterator_set_value.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_iterator_set_value.c -o htab_iterator_set_value.o

htab_clear.o: htab_clear.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_clear.c -o htab_clear.o

htab_free.o: htab_free.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c htab_free.c -o htab_free.o

############ Sdílená knihovna části ##########

htab_hash_function_shared.o: htab_hash_function.c
	$(C) $(FLAGS_C) -c -fPIC htab_hash_function.c -o htab_hash_function_shared.o
	
htab_init_shared.o: htab_init.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_init.c -o htab_init_shared.o
	
htab_move_shared.o: htab_move.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_move.c -o htab_move_shared.o

htab_size_shared.o: htab_size.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_size.c -o htab_size_shared.o
	
htab_bucket_count_shared.o: htab_bucket_count.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_bucket_count.c -o htab_bucket_count_shared.o

htab_lookup_add_shared.o: htab_lookup_add.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_lookup_add.c -o htab_lookup_add_shared.o
	
htab_begin_shared.o: htab_begin.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_begin.c -o htab_begin_shared.o
	
htab_end_shared.o: htab_end.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_end.c -o htab_end_shared.o
	
htab_iterator_next_shared.o: htab_iterator_next.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_iterator_next.c -o htab_iterator_next_shared.o
	
htab_iterator_get_key_shared.o: htab_iterator_get_key.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_iterator_get_key.c -o htab_iterator_get_key_shared.o
	
htab_iterator_get_value_shared.o: htab_iterator_get_value.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_iterator_get_value.c -o htab_iterator_get_value_shared.o
	
htab_iterator_set_value_shared.o: htab_iterator_set_value.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_iterator_set_value.c -o htab_iterator_set_value_shared.o
	
htab_clear_shared.o: htab_clear.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_clear.c -o htab_clear_shared.o
	
htab_free_shared.o: htab_free.c htab.h htab_table.h
	$(C) $(FLAGS_C) -c -fPIC htab_free.c -o htab_free_shared.o
	
############ Příkazy ############x

# Vyčištění adresáře
clean:
	-rm -f tail tail2 wordcount wordcount-dynamic libhtab.a libhtab.so *.o xkuder04.zip

# Příkaz pro udělání zip archivu
zip: 
	zip xkuder04.zip *.c *.cc *.h Makefile
