// htab_move.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// move konstruktor: vytvoření a inicializace
// nové tabulky přesunem dat z tabulky t2,
// t2 nakonec zůstane prázdná a alokovaná
// (tuto funkci cvičně použijte v programu 
// podmíněným překladem #ifdef TEST)

#include <string.h> 
#include "htab.h"
#include "htab_table.h"

htab_t *htab_move(size_t n, htab_t *from)
{
    // Nejde přesouvat když from tabulka je prázdná, a výsledné pole má mít 0 hodnot
    if (from == NULL || n == 0)
    {
        return NULL;
    }

    struct htab *tmp = htab_init(n);
    if (tmp == NULL)
    {
        return tmp;
    }
    else
    {
        htab_iterator_t record = htab_begin(from);

        while (htab_iterator_valid(record))
        {
            // Když je tam ten prvek výckrát, tak se taky musí přesunout víckrát
            for (size_t i = 0; i < record.ptr->data; i++)
            {
                // Test jestli se povedlo přidat prvek
                htab_iterator_t test_empty = htab_lookup_add(tmp, record.ptr->key);
                if (test_empty.ptr == NULL)
                {
                    return NULL;
                }

                // Přičtení počtu výskytů
                htab_iterator_set_value(test_empty, htab_iterator_get_value(test_empty) + 1);
            }

            record = htab_iterator_next(record);
        }
        
        // Vyčištění tabulky
        htab_clear(from);

        return tmp;
    }
}
