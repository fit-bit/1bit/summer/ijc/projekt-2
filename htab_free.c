// htab_free.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// destruktor: zrušení tabulky (volá htab_clear())

#include <stdlib.h>
#include "htab.h"
#include "htab_table.h"

void htab_free(htab_t * t)
{
    if (t != NULL)
    {
        htab_clear(t);
        free(t);
    }
}