// htab_bucket_count.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// vrátí počet prvků pole (.arr_size)

#include <string.h> 
#include "htab.h"
#include "htab_table.h"

size_t htab_bucket_count(const htab_t * t)
{
    return t->arr_size;
}