// htab_end.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// vrací iterátor označující (neexistující) první záznam za koncem

#include "htab.h"
#include "htab_table.h"

htab_iterator_t htab_end(const htab_t * t)
{
    // TODO netuším jestli má vracet za poslední záznam a nebo toto
    // I když ukazovat za poslední záznam zní celkem divně ¯\_(ツ)_/¯
    htab_iterator_t tmp;
    tmp.ptr = NULL;
    tmp.t = t;
    tmp.idx = t->arr_size;

    return tmp;
}