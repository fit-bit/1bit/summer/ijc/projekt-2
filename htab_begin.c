// htab_begin.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// vrací iterátor označující první záznam

#include <string.h>
#include "htab.h"
#include "htab_table.h"

htab_iterator_t htab_begin(const htab_t * t)
{
    htab_iterator_t tmp;
    size_t iterator = 0;

    // Nalezení prvního záznamu. Když tam není, vrátí iterátor na konec.
    while (iterator < t->arr_size && t->ptr[iterator] == NULL)
    {
        iterator++;
    }

    if (iterator == t->arr_size)
    {
        tmp.t = t;
        tmp.idx = iterator;
        tmp.ptr = NULL;
    }
    else
    {
        tmp.t = t;
        tmp.idx = iterator;
        tmp.ptr = t->ptr[iterator];
    }
    
    return tmp;
}