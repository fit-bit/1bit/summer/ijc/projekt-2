// htab_iterator_next.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// posun iterátoru na další záznam v tabulce (nebo na htab_end(t))

#include <string.h> 
#include "htab.h"
#include "htab_table.h"

htab_iterator_t htab_iterator_next(htab_iterator_t it)
{
    htab_iterator_t tmp;
    size_t iterator = it.idx + 1;

    // Kontrola jestli už jsme na konci tabulky
    if (it.idx == it.t->arr_size || it.ptr == NULL)
    {
        it.idx = it.t->arr_size;
        return it;
    }

    // přeskočení prázdných položek
    if (it.ptr->next == NULL)
    {
        // Následující položka už není v aktuálním seznamu
        while (iterator < it.t->arr_size && it.t->ptr[iterator] == NULL)
        {
            iterator++;
        }

        if (iterator == it.t->arr_size)
        {
            tmp.t = it.t;
            tmp.idx = iterator;
            tmp.ptr = NULL;

            return tmp;
        }

        tmp.t = it.t;
        tmp.idx = iterator;
        tmp.ptr = it.t->ptr[tmp.idx];
    }
    // Posun v seznamu na další prvek
    else
    {
        tmp.t = it.t;
        tmp.idx = it.idx;
        tmp.ptr = it.ptr->next;
    }

    return tmp;
}