// htab_lookup_add.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
//  V tabulce  t  vyhledá záznam odpovídající řetězci  key  a
//      - pokud jej nalezne, vrátí iterátor na záznam
//      - pokud nenalezne, automaticky přidá záznam a vrátí iterátor
//  Poznámka: Dobře promyslete chování této funkce k parametru key.

#include <string.h> 
#include <stdlib.h>
#include "htab.h"
#include "htab_table.h"

htab_iterator_t htab_lookup_add(htab_t * t, const char *key)
{ 
    htab_iterator_t tmp;
    size_t index;

    // vypočítání indexu do tabulky
    index = (htab_hash_function(key) % htab_bucket_count(t));
    
    //printf("Uložení slova = %s na index = %d\n", key, index);

    // Vyzkoušení nalezení záznamu a když existuje vrátí iterátor
    struct htab_item *tmp_ptr = t->ptr[index];
    while (tmp_ptr != NULL)
    {
        if (strcmp(tmp_ptr->key, key) == 0)
        {
            // poskládání iterátoru
            tmp.ptr = tmp_ptr;
            tmp.t = t;
            tmp.idx = index;

            return tmp;
        }

        tmp_ptr = tmp_ptr->next;
    }

    // Když se key nenašel, vytvoří se záznam a vrátí iterátor
    // Když se nepovede alokace, vrátí nuloví iterátor
    struct htab_item *new = malloc(sizeof(struct htab_item));
    if (new == NULL)
    {
        tmp.ptr = NULL;
        tmp.t = t;
        tmp.idx = index;
    }

    char *word = malloc(strlen(key) + 1);
    if (word == NULL)
    {
        tmp.ptr = NULL;
        tmp.t = t;
        tmp.idx = index;
    }

    strcpy(word, key);

    new->data = 0;
    new->key = word;
    new->next = NULL;

    // vytvoření záznamu v poli
    if (t->ptr[index] == NULL)
    {
        t->ptr[index] = new;
    }
    else
    {
        tmp_ptr = t->ptr[index];
        while (tmp_ptr->next != NULL)
        {
            tmp_ptr = tmp_ptr->next;
        }

        tmp_ptr->next = new;

    }

    // Zvětšení počítadla v tabulce
    t->size = t->size + 1;

    // vytvoření iterátoru
    tmp.ptr = new;
    tmp.t = t;
    tmp.idx = index;

    return tmp;
}