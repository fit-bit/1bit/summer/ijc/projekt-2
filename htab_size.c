// htab_size.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// vrátí počet prvků tabulky (.size)

#include <string.h> 
#include "htab.h"
#include "htab_table.h"

size_t htab_size(const htab_t * t)
{
    return t->size;
}