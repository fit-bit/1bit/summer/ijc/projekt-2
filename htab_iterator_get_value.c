// htab_iterator_get_value.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// vrací hodnotu, cíl musí existovat

#include "htab.h"
#include "htab_table.h"

int htab_iterator_get_value(htab_iterator_t it)
{
    if (it.ptr != NULL)
    {
        return it.ptr->data;
    }

    // Když neexistuje vrátí -1
    return -1;
}