// htab_table.h
// Řešení IJC-DU2, příklad 2), 30.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// Privátní struktura tabulky

#include <string.h> 
#include "htab.h"

#ifndef __HTAB_TABLE_H__
#define __HTAB_TABLE_H__

struct htab
{
    size_t size;
    size_t arr_size;
    struct htab_item *ptr[];
};

struct htab_item
{
    char *key;
    size_t data;
    struct htab_item *next;
};

#endif