// htab_hash_function.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// Rozptylovací funkce pro řetězce

#include <stdint.h>
#include "htab.h"

unsigned int htab_hash_function(const char *str) 
{
    uint32_t h=0;     // musí mít 32 bitů
    const unsigned char *p;

    for(p=(const unsigned char*)str; *p!='\0'; p++)
    {
        h = 65599*h + *p;
    }
    
    return h;
}