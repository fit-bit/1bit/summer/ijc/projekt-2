// tail.c
// Řešení IJC-DU2, příklad 1) a), 25.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// Napodoběnina posix příkazu tail v jazice c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

// implicitní počet posledních řádku co se má vypsat
unsigned print_lines = 10;

// 0 pro kontrolu jestli se má začínat na nějakým řádku nebo ne. Začít na 0 nejde
unsigned start_on_line = 0; 

// Macimální délka řádku
unsigned line_length_limit = 1023;

char *info_msg = "Program nemůže mít víc než 3 argumenty.\n\
Program se používá jako POSIX příkaz tail:\n\
tail soubor\n\
tail -n +3 soubor\n\
tail -n 20 <soubor";

// pro uložení načátaných řádků
struct line_structure {
    struct line_structure *next;
    char line[];
};

// Error exit
void error(char *text)
{
    fprintf(stderr, "%s\n", text);
    exit(1);
}

// Kontrola jestli to co je zadané je doopravdy číslo
int number_format(char *number)
{
    for (unsigned i=0; i < strlen(number); i++)
    {
        if (i == 0)
        {
            if (number[i] == '+')
            {
                continue;
            }
        }

        if (!(number[i] >= '0' && number[i] <= '9'))
        {
            return 1;
        }
    }

    return 0;
}

// Kontrola argumentů
FILE *check_args(int argc, char *argv[])
{
    // má se číst ze stdin a vytisknout posledních 10 řádků
    if (argc == 1)
    {
        return stdin;
    }

    // Program dostal zadaný soubor odkud má vytisknout posledních 10 řádků
    if (argc == 2)
    {
        FILE *tmp;
        tmp = fopen(argv[1], "r");
        if (tmp == NULL)
        {
            error("Nepodařilo se otevřít zadaný soubor");
        }

        return tmp;
    }

    // 2 argumenty můžou být jen -n a číslo/+číslo
    if (argc == 3)
    {
        if (strcmp(argv[1], "-n") != 0)
        {
            error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
        }

        if (number_format(argv[2]) != 0)
        {
            error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
        }

        if (argv[2][0] == '+')
        {
            start_on_line = strtol(argv[2]++ , NULL, 10);
            if (start_on_line == 0)
            {
                error("Zadané číslo musí být > 0");
            }
            return stdin;
        }
        else
        {
            print_lines = strtol(argv[2], NULL, 10);
            if (print_lines == 0)
            {
                error("Zadané číslo musí být > 0");
            }
            return stdin;
        }
    }

    // Zadaný soubor, -n a číslo/+číslo
    if (argc == 4)
    {
        if (strcmp(argv[1], "-n") == 0)
        {
            if (number_format(argv[2]) != 0)
            {
                error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
            }

            if (argv[2][0] == '+')
            {
                start_on_line = strtol(argv[2]++ , NULL, 10);
                if (start_on_line == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
            else
            {
                print_lines = strtol(argv[2], NULL, 10);
                if (print_lines == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
            
            FILE *tmp;
            tmp = fopen(argv[3], "r");
            if (tmp == NULL)
            {
                error("Nepodařilo se otevřít zadaný soubor");
            }

            return tmp;
        }
        else
        {
            if (strcmp(argv[2], "-n") != 0)
            {
                error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
            }

            if (number_format(argv[3]) != 0)
            {
                error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
            }

            if (argv[3][0] == '+')
            {
                start_on_line = strtol(argv[3]++ , NULL, 10);
                if (start_on_line == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
            else
            {
                print_lines = strtol(argv[3], NULL, 10);
                if (print_lines == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }

            // na konci proto aby když nastane chyba v předchozích podmínkách, tak aby nemusel zavírat soubor
            FILE *tmp;
            tmp = fopen(argv[1], "r");
            if (tmp == NULL)
            {
                error("Nepodařilo se otevřít zadaný soubor");
            }

            return tmp;
        }
    }

    // Nepovolený počet argumentů
    if (argc > 4)
    {
        error(info_msg);
        return NULL; // aby byl překladač spokojený
    }

    return NULL; // aby byl překladač spokojený
}

// Vymaže zbylé řádky ve struktuře
void clear_lines(struct line_structure *last_line)
{
    while (last_line != NULL)
    {
        struct line_structure *tmp = last_line;
        last_line = last_line->next;
        free(tmp);
    }
}

// Uloží všechny načtené řádky do struktury
struct line_structure *load_lines(FILE *fp)
{
    struct line_structure *old_line = NULL;
    struct line_structure *new_line = NULL;
    char tmp[line_length_limit];
    bool ower = false;

    while (fgets(tmp, line_length_limit, fp))
    {
        // kontrola jestli se řádek načet celí
        if (tmp[strlen(tmp)-1] != '\n')
        {
            //vypsání warning správy jen jednou
            if (ower == false)
            {
                // vypsání warning
                fprintf(stderr , "%s\n", "Načtený řádek je delší než 1023 znaků, zbytek řádku se zahodí");
                ower = true;
            }

            // zahození zbytku řádku
            int ch;
            while ((ch = fgetc(fp)) != '\n' && ch != EOF) 
            {
                //maze
            }

            // dání znaku nového řádku na místo
            tmp[strlen(tmp)-1] = '\n';
        }

        // naalokování struktury pro řádek a uložení
        new_line = malloc(sizeof(struct line_structure) + strlen(tmp) + 1);
        if (new_line == NULL)
        {
            // smazání už načtených řádků
            clear_lines(old_line);
            
            //uzavření souboru jestli se neště ze stdin
            if (fp != stdin)
            {
                fclose(fp);
            }
    
            error("Nepodařilo se naalokovat místo pro řádky");
        }

        new_line->next = old_line;
        strcpy (new_line->line, tmp);
        old_line = new_line;
    }

    return new_line;
}

// Vytiskne posledních n řádků ze struktury
void print_last_n_lines(struct line_structure *last_line)
{
    for (unsigned i = print_lines; i > 0; i--)
    {
        unsigned j = i;
        struct line_structure *tmp = last_line;
        while (j > 0 && tmp != NULL)
        {
            if (j == 1)
            {
                printf("%s", tmp->line);
            }
            else
            {
                tmp = tmp->next;
            }

            j--;
        }
    }
}

// Tisk řádku od nějakého zadaního čísla řádku
void start_print_on_line(FILE *fp)
{
    unsigned number_of_line = 0;
    char tmp[line_length_limit];
    bool ower = false;

    while (fgets(tmp, line_length_limit, fp))
    {
        number_of_line++;

        // kontrola jestli se řádek načet celí
        if (tmp[strlen(tmp)-1] != '\n')
        {
            //vypsání warning správy jen jednou
            if (ower == false)
            {
                // vypsání warning
                fprintf(stderr , "%s\n", "Načtený řádek je delší než 1023 znaků, zbytek řádku se zahodí");
                ower = true;
            }

            // zahození zbytku řádku
            int ch;
            while ((ch = fgetc(fp)) != '\n' && ch != EOF) 
            {
                //maze
            }

            // dání znaku nového řádku na místo
            tmp[strlen(tmp)-1] = '\n';
        }
    
        if (number_of_line >= start_on_line)
        {
            printf("%s", tmp);
        } 
    }
}


int main(int argc, char *argv[])
{
    // soubor ze kterého se má číst
    FILE *fp = check_args(argc, argv);

    // určení co se má dělat
    if (start_on_line != 0)
    {
        start_print_on_line(fp);
    }
    else
    {
        // Ukazatele na strukturu se řádky
        struct line_structure *last_line = load_lines(fp);

        // Výpis posledních n řádků
        print_last_n_lines(last_line);

        // Vyprázdnění struktury
        clear_lines(last_line);
    }

    // zavření souboru
    if (fp != stdin)
    {
        fclose(fp);
    }

    return 0;
}