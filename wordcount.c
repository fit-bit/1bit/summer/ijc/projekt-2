// wordcount.c
// Řešení IJC-DU2, příklad 2), 25.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// Předělání c++ programu do c
/**
    // wordcount-.cc
    // Použijte: g++ -std=c++11 -O2
    // Příklad použití STL kontejneru nebo unordered_map<>
    // Program počítá četnost slov ve vstupním textu,
    // slovo je cokoli oddělené "bílým znakem"

    #include <string>
    #include <iostream>
    #include <unordered_map>

    int main() {
        using namespace std;
        unordered_map<string,int> m;  // asociativní pole
        string word;

        while (cin >> word) // čtení slova
            m[word]++;      // počítání výskytů slova

        for (auto &mi: m)   // pro všechny prvky kontejneru m
            cout << mi.first << "\t" << mi.second << "\n";
            //      slovo/klíč          počet/data
    }
 **/

#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "htab.h"

// Limit délky slova dle zadání.
#define word_limit 127

// Podle stackoverflow je dobré aby v tabulce nevznikaly kolize,
// takže aby tabulka byla větší než všechny slova co tam chci dát.
// Jenže já nikdy nemám přesný počet slov.
// Dál je také prí dobré aby číslo bylo mocninou dvojky. Takže proto jsem vybral mocninu dvou 
// https://stackoverflow.com/questions/22741966/how-to-choose-size-of-hash-table
// https://www.quora.com/How-is-the-size-of-a-hash-table-determined-How-should-optimization-be-done-for-it-to-be-fast
#define hash_table_saize 32768

// Test jiné rosptilovací funkce
// djb2
#ifdef HASHTEST
unsigned int htab_hash_function(const char *str) 
{
    unsigned long hash = 5381;
    const unsigned char *p;

    for(p=(const unsigned char*)str; *p!='\0'; p++)
    {
        hash = hash*33^*p;
    }
    return hash;
}
#endif

int get_word(char *s, int max, FILE *f);

void printerr(char *s)
{
    fprintf(stderr, "%s\n", s);
    exit(1);
}

int main()
{
    bool printed_err = false;

    char word[word_limit] = "";
    htab_t *hash_table;

    // Inicializace tabulky
    hash_table = htab_init(hash_table_saize);
    if (hash_table == NULL)
    {
        printerr("Nepodařilo se inicializovat hashovací tabulku");
    }

    // načtení slov do tabulky
    int returned;
    htab_iterator_t test_empty;
    while ((returned = get_word(word, word_limit, stdin)) != EOF)
    {
        if (returned == -2)
        {
            if (printed_err == false)
            {
                fprintf(stderr, "%s\n", "Některé zadané slovo/va bylo/la moc dlouhé/há, takže se zkrátilo/la na 127 znaků");
                printed_err = true;
            }
        }

        // Kontrola jestli se slovo přidalo
        test_empty = htab_lookup_add(hash_table, word);
        if (test_empty.ptr == NULL)
        {
            htab_free(hash_table);
            printerr("Nepodařilo se uložit některé slovo do tabulky");
        }

        // Přičtení počtu výskytů
        htab_iterator_set_value(test_empty, htab_iterator_get_value(test_empty) + 1);

        memset(word,0,strlen(word));
    }

    // když na konci souboru před EOF bylo slovo, tak se musí zapsat taky
    if (word[0] != '\0')
    {
        test_empty = htab_lookup_add(hash_table, word);
        if (test_empty.ptr == NULL)
        {
            htab_free(hash_table);
            printerr("Nepodařilo se uložit některé slovo do tabulky");
        }
    }

    // test přesunu tabulky
    #ifdef TEST
        htab_t *hash_table_test = htab_move(12536, hash_table);
        if (hash_table_test == NULL)
        {
            htab_free(hash_table_test);
            htab_free(hash_table);
            printerr("Nepodařilo se uložit některé slovo do tabulky kam se přesouvolo");
        }

        // Vypsání tabulky
        htab_iterator_t record = htab_begin(hash_table_test);

        while (htab_iterator_valid(record))
        {
            printf("%s\t%d\n", htab_iterator_get_key(record), htab_iterator_get_value(record));
            record = htab_iterator_next(record);
        }

        htab_free(hash_table_test);
        htab_free(hash_table);
        return 0;
    #else
        // Vypsání tabulky
        htab_iterator_t record = htab_begin(hash_table);

        while (htab_iterator_valid(record))
        {
            printf("%s\t%d\n", htab_iterator_get_key(record), htab_iterator_get_value(record));
            record = htab_iterator_next(record);
        }

        htab_free(hash_table);
        return 0;
    #endif
}