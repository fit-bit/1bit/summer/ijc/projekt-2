// htab_init.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// konstruktor: vytvoření a inicializace tabulky
// numb = počet prvků pole (.arr_size)

#include <string.h> 
#include <stdlib.h>
#include "htab.h"
#include "htab_table.h"

// Tento soubor se použije vždy a inline funkce potřebují externy
extern inline bool htab_iterator_valid(htab_iterator_t it);
extern inline bool htab_iterator_equal(htab_iterator_t it1, htab_iterator_t it2);

htab_t *htab_init(size_t n)
{
    // TODO nevím jestli to tak má být, ale vypadá to logicky
    if (n == 0)
    {
        return NULL;
    }

    // naalokování struktury
    struct htab *tmp = malloc(sizeof(htab_t) + n*sizeof(struct htab_item *));
    if (tmp == NULL)
    {
        return tmp;
    }
    else
    {
        tmp->size = 0;
        tmp->arr_size = n;

        // nastavení ukazatelů v poli na null
        for (size_t i = 0; i < n; i++)
        {
            tmp->ptr[i] = NULL;
        }

        return tmp;
    }
}
