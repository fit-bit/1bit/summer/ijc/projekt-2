// htab_iterator_get_key.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// vrací klíč, cíl musí existovat

#include "htab.h"
#include "htab_table.h"

const char * htab_iterator_get_key(htab_iterator_t it)
{
    if (it.ptr != NULL)
    {
        return it.ptr->key;
    }

    // Když neexistuje vrátí ""
    return "";
}