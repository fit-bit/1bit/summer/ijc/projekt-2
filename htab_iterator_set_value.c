// htab_iterator_set_value.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// přepisuje hodnotu, cíl musí existovat

#include "htab.h"
#include "htab_table.h"

int htab_iterator_set_value(htab_iterator_t it, int val)
{
    // Když se povede vrací 0

    if (it.ptr != NULL)
    {
        it.ptr->data = val;
        return 0;
    }

    return -1;
}