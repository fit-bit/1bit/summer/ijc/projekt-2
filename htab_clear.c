// htab_clear.c
// Řešení IJC-DU2, příklad 2), 29.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// zrušení všech položek, tabulka zůstane prázdná

#include <string.h> 
#include <stdlib.h>
#include "htab.h"
#include "htab_table.h"

void htab_clear(htab_t * t)
{
    if (t != NULL)
    {
        // projití celé tabulky
        for (size_t i = 0; i < htab_bucket_count(t); i++)
        {
            // projití jednotlivých seznamů a smazání všech záznamů
            struct htab_item *tmp_ptr = t->ptr[i];
            struct htab_item *tmp_ptr2;
            while (tmp_ptr != NULL)
            {
                tmp_ptr2 = tmp_ptr->next;
                free(tmp_ptr->key);
                free(tmp_ptr);
                tmp_ptr = tmp_ptr2;
            }
            
            t->ptr[i] = NULL;
        }

        t->size = 0;
    }
}