// tail2.cc
// Řešení IJC-DU2, příklad 1) b), 25.3.2019
// Autor: Matěj Kudera, FIT
// Přeloženo: gcc 7.4
// 	          g++ 7.4
// Napodoběnina posix příkazu tail v jazice c++

#include <iostream>
#include <string>
#include <fstream>
#include <queue>

// implicitní počet posledních řádku co se má vypsat
unsigned print_lines = 10;

// 0 pro kontrolu jestli se má začínat na nějakým řádku nebo ne. Začít na 0 nejde
unsigned start_on_line = 0; 

std::string info_msg = "Program nemůže mít víc než 3 argumenty.\nProgram se používá jako POSIX příkaz tail:\ntail soubor\ntail -n +3 soubor\ntail -n 20 <soubor";

// Error exit
void error(std::string text)
{
    std::cerr << text << std::endl;
    exit(1);
}

// Kontrola jestli je číslo ve formátu který má být zadaný
int number_format(std::string number)
{
    for (unsigned i=0; i < number.length(); i++)
    {
        if (i == 0)
        {
            if (number[i] == '+')
            {
                continue;
            }
        }

        if (!(number[i] >= '0' && number[i] <= '9'))
        {
            return 1;
        }
    }

    return 0;
}

// Kontrola argumentů
std::string check_args(int argc, char *argv[])
{
    // má se číst ze stdin a vytisknout posledních 10 řádků
    if (argc == 1)
    {
        return "";
    }

    // Program dostal zadaný soubor odkud má vytisknout posledních 10 řádků
    if (argc == 2)
    {
        return argv[1];
    }

    // 2 argumenty můžou být jen -n a číslo/+číslo
    if (argc == 3)
    {
        if (((std::string)argv[1]).compare("-n") != 0)
        {
            error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
        }

        if (number_format(argv[2]) != 0)
        {
            error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
        }

        if (argv[2][0] == '+')
        {
            start_on_line = std::stol(argv[2]++ , nullptr, 10);
            if (start_on_line == 0)
            {
                error("Zadané číslo musí být > 0");
            }
            return "";
        }
        else
        {
            print_lines = std::stol(argv[2]++ , nullptr, 10);
            if (print_lines == 0)
            {
                error("Zadané číslo musí být > 0");
            }
            return "";
        }
    }

    // Zadaný soubor, -n a číslo/+číslo
    if (argc == 4)
    {
        if (((std::string)argv[1]).compare("-n") == 0)
        {
            if (number_format(argv[2]) != 0)
            {
                error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
            }

            if (argv[2][0] == '+')
            {
                start_on_line = std::stol(argv[2]++ , nullptr, 10);
                if (start_on_line == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
            else
            {
                print_lines = std::stol(argv[2]++ , nullptr, 10);
                if (print_lines == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
            
            return argv[3];
        }
        else
        {
            if (((std::string)argv[2]).compare("-n") != 0)
            {
                error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
            }

            if (number_format(argv[3]) != 0)
            {
                error("Když jsou zadány 2 argumenty, první musí být -n a druhý číslo nebo +číslo");
            }

            if (argv[3][0] == '+')
            {
                start_on_line = std::stol(argv[3]++ , nullptr, 10);
                if (start_on_line == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
            else
            {
                print_lines = std::stol(argv[3]++ , nullptr, 10);
                if (print_lines == 0)
                {
                    error("Zadané číslo musí být > 0");
                }
            }
    
            return argv[1];
        }
    }

    // Nepovolený počet argumentů
    if (argc > 4)
    {
        error(info_msg);
        return ""; // aby byl překladač spokojený
    }

    return ""; // aby byl překladač spokojený
}

// Tisk řádku od nějakého zadaního čísla řádku
void start_print_on_line(std::istream *fp)
{
    unsigned number_of_line = 0;
    std::string tmp;

    while (std::getline(*fp, tmp))
    {
        number_of_line++;

        if (number_of_line >= start_on_line)
        {
            std::cout << tmp << std::endl;
        } 
    }
}

// Vytiskne posledních n řádků ze struktury
void print_last_n_lines(std::istream *fp)
{
    std::queue<std::string> lines;
    std::string tmp;
    long pop_count;

    // načtení řádků do zásobníku
    while (std::getline(*fp, tmp))
    {
        lines.push(tmp);
    }

    // nachistání řádku pro výpis
    pop_count = lines.size() - print_lines;
    while (pop_count > 0)
    {
        lines.pop();
        pop_count--;
    }

    // vypsání prvků
    while (!lines.empty())
    {
        std::cout << lines.front() << std::endl;
        lines.pop();
    }
}

int main(int argc, char *argv[])
{
    // Podle zadání pro zrychlení
    std::ios::sync_with_stdio(false);

    // soubor ze kterého se má číst
    std::string filename = check_args(argc, argv);
    std::istream *fp;
    std::ifstream tmp;

    // Určení odkud se mají brát řádky
    if (filename.empty())
    {
        fp = &(std::cin);
    }
    else
    {
        tmp.open(filename, std::ifstream::in);

        if (!tmp.is_open())
        {
            error("Nepadařilo se otevřít soubor pro čtení");
        }

        fp = &tmp;
    }

    // určení co se má dělat
    if (start_on_line != 0)
    {
        start_print_on_line(fp);
    }
    else
    {
        print_last_n_lines(fp);
    }

    // zavření souboru
    if (tmp.is_open())
    {
        tmp.close();
    }

    return 0;
}